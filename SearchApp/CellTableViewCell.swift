//
//  CellTableViewCell.swift
//  SearchApp
//
//  Created by Syncrhonous on 30/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class CellTableViewCell: UITableViewCell {
    
    

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
