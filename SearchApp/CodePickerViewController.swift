//
//  ViewController.swift
//  SearchApp
//
//  Created by Syncrhonous on 30/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class CodePickerViewController: UIViewController {

    //var delegate: CustomAlertViewDelegate?
    // var selectedOption = "First"
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    
    let countryCodeArr = ["+93", "+355", "+213", "+1", "+376", "+244", "+1", "+672", "+1", "+54",  "+374", "+297", "+61", "+43", "+994", "+1", "+973", "+880", "+1", "+375", "+32", "+501", "+229", "+1", "+975", "+591", "+387", "+267", "+55", "+1", "+673", "+359", "+226", "+257", "+855", "+237", "+1", "+238", "+1", "+236", "+235", "+56", "+86", "+61", "+57", "+269", "+242", "+243", "+682", "+506", "+385", "+53", "+357", "+420", "+45", "+253", "+1", "+1", "+593", "+20", "+503", "+240", "+291", "+372", "+251", "+500", "+298", "+679", "+358", "+33", "+594", "+689", "+241", "+220", "+995", "+49", "+233", "+350", "+30", "+299", "+1", "+590", "+1", "+502", "+44", "+224", "+245", "+592", "+509", "+379", "+504", "+852", "+36", "+354", "+91", "+62", "+98", "+964", "+353", "+44", "+972", "+39", "+1", "+81", "+44", "+962", "+7", "+254", "+686", "+383", "+965", "+996", "+856", "+371", "+961", "+266", "+231", "+218", "+423", "+370", "+352", "+853", "+389", "+261", "+265", "+60", "+960", "+223", "+356", "+692", "+596", "+222", "+230", "+262", "+52", "+691", "+373", "+377", "+976", "+382", "+1", "+212", "+258", "+95", "+264", "+674", "+977", "+31", "+687", "+64", "+505", "+227", "+234", "+683", "+672", "+850", "+1", "+47", "+968", "+92", "+680", "+970", "+507", "+675", "+595", "+51", "+63", "+870", "+48", "+351", "+1", "+974", "+262", "+40", "+7", "+250", "+590", "+1", "+1", "+590", "+508",  "+1", "+685", "+378", "+239", "+966", "+221", "+381", "+248", "+232", "+65", "+1", "+421", "+386", "+677", "+252", "+27", "+82", "+34", "+94", "+249", "+597", "+268", "+46", "+41", "+963", "+886", "+992", "+255", "+66", "+670", "+228", "+690", "+676", "+1", "+216", "+90", "+993", "+1", "+688", "+256", "+380", "+971", "+44", "+1", "+598", "+1", "+998", "+678", "+58", "+84", "+681", "+967",  "+260", "+263"]
    

    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tbView: UITableView!
    
    let countryNameArr = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Brazil", "British Virgin Island", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Colombia", "Comoros", "Congo", "Congo The Democratic Republic of The", "Cook Islands", "Costarica",  "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands(Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Holy See(Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran(Islamic Republic of)", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati","Kosovo", "Kuwait", "Kyrgyzstan", "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia Federated States of", "Moldova Republic of", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco","Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island","North Korea", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Sanit Barthelemy","Saint Kitts and Nevis", "Saint Lucia","Saint Martin", "Saint Pierre and Miquelon",  "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania United Republic of", "Thailand","Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Virgin Islands", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam",  "Wallis and Futuna Islands", "Yemen",  "Zambia", "Zimbabwe"]
    
    
    
    //north = North Korea, south = South Korea, united = United Arab Emirates (UAE), sri = Sri Lanka
    let countryFlagArr =  ["Afghanistan","Albania", "Algeria", "American_Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua_and_Barbuda", "Argentina",  "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus","Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia_and_Herzegowina", "Botswana", "Brazil", "British_Virgin_Island", "Brunei_Darussalam", "Bulgaria", "Burkina_Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape_Verde","Cayman_Islands", "Central_African_Republic", "Chad", "Chile", "China", "Christmas_Island", "Colombia", "Comoros", "Congo", "Congo_The_Democratic_Republic_of_The", "Cook_Islands", "Costarica",  "Croatia", "Cuba", "Cyprus", "Czech_Republic", "Denmark", "Djibouti", "Dominica", "Dominican_Republic","Ecuador", "Egypt", "El_Salvador", "Equatorial_Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland_Islands", "Faroe_Islands", "Fiji", "Finland", "France", "French_Guiana", "French_Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea_Bissau", "Guyana", "Haiti", "Holy_See", "Honduras", "Hong_Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle_of_Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati","Kosovo", "Kuwait", "Kyrgyzstan", "Lao_People's_Democratic_Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia_The_Former_Yugoslav_Republic_of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall_Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia_Federated_States_of", "Moldova_Republic_of", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New_Caledonia", "New_Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk_Island","North_Korea", "Northern_Mariana_Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua_New_Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto_Rico", "Qatar", "Reunion", "Romania", "Russian_Federation", "Rwanda", "Sanit_Barthelemy", "Saint_Kitts_and_Nevis", "Saint_Lucia", "Saint_Martin", "Saint_Pierre_and_Miquelon",  "Saint_Vincent_and_the_Grenadines", "Samoa", "San_Marino", "Sao_Tome_and_Principe", "Saudi_Arabia", "Senegal", "Serbia", "Seychelles", "Sierra_Leone", "Singapore", "Sint_Maarten", "Slovakia", "Slovenia", "Solomon_Islands", "Somalia", "South_Africa", "South_Korea", "Spain", "Sri_Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syrian_Arab_Republic", "Taiwan", "Tajikistan", "Tanzania_United_Republic_of", "Thailand","Timor_Leste", "Togo", "Tokelau", "Tonga", "Trinidad_and_Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks_and_Caicos_Islands", "Tuvalu", "Uganda", "Ukraine", "United_Arab_Emirates", "United_Kingdom", "United_States", "Uruguay", "US_Virgin_Islands", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam",  "Wallis_and_Futuna_Islands", "Yemen",  "Zambia", "Zimbabwe"]
    
    
    
    var searchedCountry = [String]()
    var searchedCountryCode = [String]()
    var searchedCountryFlag = [String]()
    var isSearching = false
    var selector = 0
    var arrs = [String]()
    let defaults = UserDefaults.standard

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        //tbView.separatorStyle = .none
    }
}

extension CodePickerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return searchedCountry.count
        } else {
            return countryNameArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CellTableViewCell
     
        if isSearching {
            cell?.nameLabel.text = searchedCountry[indexPath.row]
            cell?.codeLabel.text = searchedCountryCode[indexPath.row]
            print("searchedCountryFlag: \(searchedCountryFlag)")
            cell?.img.image = UIImage(named: searchedCountryFlag[indexPath.row].lowercased())
        } else {
            cell?.nameLabel.text = countryNameArr[indexPath.row]
            cell?.codeLabel.text = countryCodeArr[indexPath.row]
            cell?.img.image = UIImage(named: countryFlagArr[indexPath.row].lowercased())
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selector=indexPath.row
        var selectedCountry: String = "Bangladesh"
        var selectedCountryCode: String = "+880"
        if isSearching {
            selectedCountryCode = searchedCountryCode[selector]
            selectedCountry = searchedCountry[selector]
            print("selector: \(searchedCountryCode[selector]) = \(selectedCountryCode)")
        }else{
            selectedCountryCode = countryCodeArr[selector]
            selectedCountry = countryNameArr[selector]
        }
        var firstWordOfCountry = "bangladesh"
        
        firstWordOfCountry = selectedCountry.split(separator: "(").first.map(String.init)!.lowercased()
        var selectedCountryF = firstWordOfCountry.replacingOccurrences(of: " ", with: "_")
        selectedCountryF = selectedCountryF.replacingOccurrences(of: "-", with: "_")
        
        print("selectedCountry name: \(selectedCountry)")
        print("selectedCountry new stting: \(selectedCountryF)")
        print("selectedCountry phone code: \(selectedCountryCode)")
        
        
        defaults.set(selectedCountry, forKey: "CountryName")
        defaults.set(selectedCountryF, forKey: "CountryFlag")
        defaults.set(selectedCountryCode, forKey: "CountryCode")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refresh"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension CodePickerViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var stext: String = ""
        print("prefix: \((searchText.prefix(1) as NSString).integerValue)")
        if ((searchText.prefix(1) as NSString).integerValue > 0 && (searchText.prefix(1) as NSString).integerValue <= 9){
            stext = "+" + searchText
        }else{
            stext = searchText
        }

        print("ssssssssssssssssss:\(stext)")
       
        
        if stext.prefix(1).lowercased() == "+"  {
            searchedCountryCode = countryCodeArr.filter({$0.prefix(stext.count) == stext})
            if stext.count > 0 {
                isSearching = true
            }else{
                isSearching = false
            }
            searchedCountry = Array<String>(repeating: "", count: searchedCountryCode.count)
            searchedCountryFlag = Array<String>(repeating: "", count: searchedCountryCode.count)
            if searchedCountryCode.count > 0 {
                var p:Int = 0;
                for i in 0...searchedCountryCode.count-1{
                
                    for j in p...countryCodeArr.count-1{
                        if searchedCountryCode[i] == countryCodeArr [j] {
                            
                            searchedCountry[i] = countryNameArr[j]
                            searchedCountryFlag[i] = countryFlagArr[j]
                            p = j+1
                            break
                        }
                    }
                }
            }
        }else{
            searchedCountry = countryNameArr.filter({$0.lowercased().prefix(stext.count) == stext.lowercased()})
            if stext.count > 0 {
                isSearching = true
            }else{
                isSearching = false
            }
            searchedCountryCode = Array<String>(repeating: "", count: searchedCountry.count)
            searchedCountryFlag = Array<String>(repeating: "", count: searchedCountry.count)
            if searchedCountry.count > 0 {
                for i in 0...searchedCountry.count-1{
                    for j in 0...countryNameArr.count-1{
                        if searchedCountry[i] == countryNameArr [j] {
                            searchedCountryCode[i] = countryCodeArr[j]
                            searchedCountryFlag[i] = countryFlagArr[j]
                        }
                    }
                }
            }
        }

        tbView.reloadData()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}
