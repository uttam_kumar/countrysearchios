//
//  InitialViewController.swift
//  SearchApp
//
//  Created by Syncrhonous on 30/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    
    
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var countryCode: UIButton!
    let defaults = UserDefaults.standard
    
    
    
    @objc func refreshLbl(notification: NSNotification) {
        let countryPicker = defaults.string(forKey: "CountryName") ?? "Bangladesh"
        let countryPickerCode = defaults.string(forKey: "CountryCode") ?? "+880"
        countryNameLabel.text = "\(countryPicker) (\(countryPickerCode))"
        let countryPickerFlag = defaults.string(forKey: "CountryFlag") ?? "bangladesh"
        countryImageView.image = UIImage(named: countryPickerFlag)
        print(countryPickerFlag)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        countryNameLabel.text = "Bangladesh (+880)"
        countryImageView.image = UIImage(named: "bangladesh")
        
    
        let countryPicker = UITapGestureRecognizer(target: self, action: #selector(countryPickerViewAction(_:)))
        self.countryPickerView.addGestureRecognizer(countryPicker)
        NotificationCenter.default.addObserver(self,selector: #selector(refreshLbl), name: NSNotification.Name(rawValue:  "refresh"),object: nil)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func selectCountryCode(_ sender: UIButton) {

    }
    
    
    
    //video bitrate view clicked
    @objc func countryPickerViewAction(_ sender:UITapGestureRecognizer){
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "countryCodePicker") as! CodePickerViewController
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        //customAlert.delegate = self
        self.present(customAlert, animated: true, completion: nil)
    }
    
    
    
}
